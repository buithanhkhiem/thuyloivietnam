# %%
import requests
import pandas as pd
# pd.set_option("max_rows", 99999)
pd.options.display.max_rows = 999
import datetime

def get_info_hothuydien(time):
    # payload = {'ishothuydien':'2','time': '2022-02-15 00:00:00,000'}
    payload = {'ishothuydien':'2','time': str(time) + ',000'}
    url = 'http://e15.thuyloivietnam.vn/CanhBaoSoLieu/ATCBMNHo'
    r = requests.post(url,data = payload)
    response_data = r.json() 

    df = pd.DataFrame(response_data, columns=['LakeName','ProvinceName','TdMucNuoc'])
    df.insert(0,'Ngày Tháng',time.strftime("%Y/%m/%d"))
    return df 

rageDate = pd.date_range(end=datetime.datetime.today().strftime("%Y/%m/%d"), periods=7)
rageDate

data_frame_7_day = pd.DataFrame()

for itemRageDate in rageDate:
    # data_frame_7_day.concat(get_info_hothuydien(itemRageDate)) 
    data_frame_7_day = pd.concat([data_frame_7_day,get_info_hothuydien(itemRageDate)])
data_frame_7_day

# %%
print(datetime.datetime.today())

# %%
print(datetime.datetime.now())

# %%



